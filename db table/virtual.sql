-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2018 at 02:57 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `virtual`
--

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `class_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `class_topic_name` varchar(255) NOT NULL,
  `class_text_document` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`class_id`, `course_id`, `class_topic_name`, `class_text_document`, `video`, `created_at`, `update_at`, `status`) VALUES
(12, 3, 'Matthew Merrill', 'Temporibus alias atque aliquam est corporis blanditiis ullamco animi odit consequatur quas enim velit facere dolor repellendus Quo voluptas', 'kesi_ke_dil_me_rehna_tha____shamshad_begam____lata_mangeshkar___film_babul.mp4', '2018-09-22 09:36:30', '0000-00-00 00:00:00', 0),
(13, 3, 'Thaddeus Terrell', 'Sint magnam distinctio Est lorem quis omnis tempora id omnis qui nihil non adipisci', 'Bhawein_bol_te_bhawein_na_bol-_do_laachian_(1959).mp4', '2018-09-22 11:09:45', '0000-00-00 00:00:00', 0),
(14, 2, 'Josephine Jimenez', 'Est molestias enim deleniti et quia provident magnam incididunt non quibusdam exercitationem tempora', 'Bhawein_bol_te_bhawein_na_bol-_do_laachian_(1959).mp4', '2018-09-22 15:03:27', '0000-00-00 00:00:00', 0),
(15, 10, 'Jerry Randall', 'Consequat Ad aut voluptatem accusamus et obcaecati', 'Bhawein_bol_te_bhawein_na_bol-_do_laachian_(1959).mp4', '2018-09-22 16:29:58', '0000-00-00 00:00:00', 0),
(16, 3, 'Winter Crawford', 'Quidem aut unde debitis culpa ad ut ut veritatis reprehenderit et corporis', 'Bhawein_bol_te_bhawein_na_bol-_do_laachian_(1959).mp4', '2018-09-24 09:26:26', '0000-00-00 00:00:00', 0),
(17, 3, 'Kenyon Davidson', 'Delectus dolor laborum Mollit tempora velit', 'Bhawein_bol_te_bhawein_na_bol-_do_laachian_(1959).mp4', '2018-09-24 09:36:19', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `course_id` int(11) NOT NULL,
  `course_title` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`course_id`, `course_title`, `created_at`, `update_at`, `status`) VALUES
(2, 'OOP', '2018-09-21 16:08:31', '2018-09-22 06:27:18', 0),
(3, 'Web Development', '2018-09-21 16:08:31', '0000-00-00 00:00:00', 0),
(6, 'SPM', '2018-09-21 16:08:31', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE `exam` (
  `exam_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `exam_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam`
--

INSERT INTO `exam` (`exam_id`, `course_id`, `exam_title`) VALUES
(1, 3, 'mid_term'),
(10, 3, 'End_term');

-- --------------------------------------------------------

--
-- Table structure for table `multiplechoices`
--

CREATE TABLE `multiplechoices` (
  `multiple_choice_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `choice_a` varchar(255) NOT NULL,
  `choice_b` varchar(255) NOT NULL,
  `choice_c` varchar(255) NOT NULL,
  `choice_d` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `multiplechoices`
--

INSERT INTO `multiplechoices` (`multiple_choice_id`, `question_id`, `choice_a`, `choice_b`, `choice_c`, `choice_d`, `answer`, `status`, `created_at`, `updated_at`) VALUES
(2, 6, 'Personal Hypertext Processor', 'Private Home Page', 'PHP: Hypertext Preprocessor', 'None', 'C', 0, '2018-09-22 13:56:55', '2018-09-24 08:31:16'),
(3, 7, ' <script>...</script>', ' <?php>...</?>', '<?php...?>', '<&>...</&>', 'A', 0, '2018-09-22 13:58:16', '2018-09-24 08:31:02'),
(4, 8, 'Ea voluptas eos pariatur Aut molestiae consequatur Amet neque voluptatem maiores molestiae quas autem', 'Fugit culpa ut ab eum in nulla dolorem nostrud excepturi ducimus autem ut maxime cupiditate', 'Autem qui minus id ut quam', 'Laboriosam dolore voluptas a similique quia consequatur dolorem lorem qui et eu et voluptate enim ipsum ullamco occaecat veritatis', 'B', 0, '2018-09-22 14:05:48', '2018-09-24 08:31:06'),
(5, 9, 'Totam qui quisquam voluptatem expedita nihil corrupti natus soluta unde', 'Aut accusantium harum mollitia mollit id in irure et laborum Qui', 'Aut consequat Ut cupidatat eos nihil', 'Deserunt veniam reprehenderit ab fuga Dolore odit quos cum eveniet', 'D', 0, '2018-09-22 14:33:29', '2018-09-24 08:31:19'),
(6, 10, 'Nihil molestiae suscipit dolores laboriosam eum autem amet', 'Numquam aut vel velit atque ex nisi eligendi', 'In rem asperiores voluptates porro sed aut laborum Voluptas omnis illum et non', 'Aspernatur ex doloremque deserunt necessitatibus in dolor eum autem quod eveniet ex laudantium nostrud eiusmod non voluptatem', 'A', 0, '2018-09-22 14:35:34', '2018-09-24 08:31:10'),
(7, 11, 'hello text ', 'hy text', 'Hyper Text Markup Language', 'None', 'C', 0, '2018-09-24 09:04:10', '0000-00-00 00:00:00'),
(8, 12, 'hello text ', 'hy text', 'Hyper Text Markup Language', 'None', 'C', 0, '2018-09-24 09:04:59', '0000-00-00 00:00:00'),
(9, 13, 'Culpa fugit aliquam perferendis tenetur totam cupidatat minus delectus quo', 'Totam vero earum et aute occaecat minus magna ex sit aut et libero sint ex iusto eveniet', 'Placeat voluptas amet dolorem est sit est', 'Iure nostrud in sapiente Nam dolorem totam nostrum molestiae iusto voluptas harum et mollit debitis eius', 'B', 0, '2018-09-24 09:05:36', '0000-00-00 00:00:00'),
(10, 14, 'Modi explicabo Fugiat facere nihil voluptatibus ratione saepe qui sed vero placeat aliqua Sit dicta sunt vel nesciunt', 'Ipsa nulla est pariatur Earum sed dolore necessitatibus exercitationem eveniet animi velit recusandae', 'Id quasi consequuntur quis tempor et sit ut ipsum cumque expedita exercitationem', 'Ut ipsum et esse aperiam voluptatibus magni labore tempore in consequatur aliquip a ratione voluptatem amet', 'B', 0, '2018-09-24 09:07:05', '0000-00-00 00:00:00'),
(11, 15, 'Optio culpa exercitation accusamus saepe non hic dolore eveniet mollit nisi esse quod', 'Fugiat aut et quia dicta sit amet aliquam officiis et reprehenderit incidunt ut veniam in ut sunt et maxime quas', 'Beatae dolor alias quasi dolor voluptatem velit nulla commodo', 'Labore deserunt voluptate aliquid ut voluptas et tempore sunt', 'D', 0, '2018-09-24 09:32:52', '0000-00-00 00:00:00'),
(12, 16, 'Vel duis veniam provident quis ullamco voluptatum voluptate', 'Eum quia et voluptas quis fuga Sit ipsum quo', 'Laborum Sit accusamus et quod eius laborum Saepe vel deserunt non sint est non voluptas voluptates alias sapiente cupidatat optio', 'Sapiente ea error Nam nulla dolor elit sint assumenda dolorem molestiae libero sed exercitationem recusandae Tenetur qui et', 'C', 0, '2018-09-24 09:36:45', '0000-00-00 00:00:00'),
(13, 17, 'Blanditiis repellendus Praesentium corporis omnis unde asperiores sit dolor sint quia dolor proident quia aut aut', 'Nam in voluptatibus dolores sint ullamco fuga Quia eos irure voluptas amet non aut incidunt optio', 'Quo omnis ut reprehenderit qui sit magna dolor adipisci pariatur', 'Nulla necessitatibus reprehenderit totam quasi et voluptatem dolor sapiente quis consequat Amet', 'C', 0, '2018-09-24 10:52:47', '0000-00-00 00:00:00'),
(14, 18, 'Vel ratione incididunt nisi similique atque', 'Sit alias dolores in quo Nam et facilis et sed iste eius dolores sunt esse dolore optio ducimus dolores delectus', 'Ullam aut enim velit a veritatis facilis id necessitatibus dolore', 'Laudantium consequatur rerum lorem laborum Eveniet dignissimos autem voluptas ea soluta qui debitis aperiam excepturi ut omnis omnis', 'B', 0, '2018-09-24 10:52:53', '0000-00-00 00:00:00'),
(15, 19, 'Autem est veniam placeat eveniet dolore veniam Nam unde Nam illo aut quis aspernatur aliquid reprehenderit', 'Velit velit ad dolor velit', 'Vero nihil veniam quis tempore sapiente dolores', 'Aut commodi quidem enim ipsa est eos at deleniti qui repellendus Dolor reprehenderit commodo', 'C', 0, '2018-09-24 10:52:58', '0000-00-00 00:00:00'),
(16, 20, 'Laboriosam id in dolorum libero quae quam similique aspernatur', 'Placeat nostrum in voluptas reiciendis et molestiae placeat', 'In in dolorem do minus qui', 'Quia aliquip ut rerum incididunt nostrud eum praesentium dolores exercitation commodo libero reiciendis sit laborum', 'B', 0, '2018-09-24 10:53:06', '0000-00-00 00:00:00'),
(17, 21, 'Minim est consequatur impedit consequat Quis vero minima rerum animi', 'Qui labore laboriosam aut irure eius proident ullam', 'Ea quam atque laboriosam ut anim maxime qui', 'Doloremque laborum laudantium labore quia ipsum aut dolores eveniet placeat labore', 'B', 0, '2018-09-24 10:54:00', '0000-00-00 00:00:00'),
(18, 22, 'Minim est consequatur impedit consequat Quis vero minima rerum animi', 'Qui labore laboriosam aut irure eius proident ullam', 'Ea quam atque laboriosam ut anim maxime qui', 'Doloremque laborum laudantium labore quia ipsum aut dolores eveniet placeat labore', 'B', 0, '2018-09-24 11:04:59', '0000-00-00 00:00:00'),
(19, 23, 'Rerum dolor dolor dignissimos consequuntur laborum In officiis aute error nostrud voluptatem ratione eius eaque', 'Doloremque fugit labore quo enim temporibus similique et expedita reprehenderit quae nisi laborum In sit occaecat quibusdam culpa', 'Deserunt sint nobis aut optio expedita dignissimos', 'Necessitatibus et ipsa nihil quia dolore sit est magni eos illum neque commodi', 'B', 0, '2018-09-24 11:05:06', '0000-00-00 00:00:00'),
(20, 24, 'Ex debitis error quibusdam enim et voluptatem atque facere et et sit iusto magnam mollitia', 'Esse ad nobis ut veniam eu qui quod', 'Nisi blanditiis voluptatum minima minim hic nisi saepe in distinctio Quia', 'Ut dolore minim qui est incidunt et ipsum veritatis elit deserunt sint eius', 'C', 0, '2018-09-24 11:05:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `question_title` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`question_id`, `course_id`, `quiz_id`, `exam_id`, `question_title`, `status`, `created_at`, `updated_at`) VALUES
(6, 3, 3, 0, 'What does PHP stand for?', 0, '2018-09-22 13:56:55', '2018-09-22 16:54:10'),
(7, 3, 3, 0, 'PHP server scripts are surrounded by delimiters, which?', 0, '2018-09-22 13:58:16', '2018-09-22 16:54:32'),
(8, 3, 3, 0, 'Pariatur Quia beatae dolore facere quam aliquip deserunt non blanditiis aut occaecat ea provident sapiente minima voluptate molestias non', 0, '2018-09-22 14:05:48', '2018-09-22 16:54:38'),
(9, 3, 1, 0, 'Sed sit nostrud minus aliquip eum deleniti est deserunt corporis', 0, '2018-09-22 14:33:29', '2018-09-22 16:55:02'),
(10, 3, 2, 0, 'Neque dolore ullamco odit consequatur voluptas delectus est eu cum possimus praesentium quo quia est fugit ad qui', 0, '2018-09-22 14:35:34', '2018-09-22 16:55:05'),
(11, 3, 2, 0, 'what is Html', 0, '2018-09-24 09:04:10', '0000-00-00 00:00:00'),
(12, 3, 2, 0, 'what is Html', 0, '2018-09-24 09:04:59', '0000-00-00 00:00:00'),
(13, 3, 2, 0, 'Sed et ea ad soluta officiis et qui ut impedit harum ut', 0, '2018-09-24 09:05:36', '0000-00-00 00:00:00'),
(14, 3, 2, 0, 'Repellendus Eum quia fugiat veritatis dolor voluptatem', 0, '2018-09-24 09:07:05', '0000-00-00 00:00:00'),
(15, 3, 3, 0, 'Expedita vero architecto pariatur Neque dolor sit nulla ad cupidatat incidunt odio odio unde', 0, '2018-09-24 09:32:52', '0000-00-00 00:00:00'),
(16, 3, 9, 0, 'Repudiandae enim qui autem exercitation aliqua Ut sed sit sunt dolore qui ut ut quisquam commodi adipisci et nihil optio', 0, '2018-09-24 09:36:45', '0000-00-00 00:00:00'),
(17, 3, 0, 1, 'Qui quos architecto porro quasi magna rem recusandae Officia placeat hic velit animi cum excepturi quas nulla tempor', 0, '2018-09-24 10:52:47', '0000-00-00 00:00:00'),
(18, 3, 0, 1, 'Libero ratione irure similique magna sit in corrupti tempora labore facilis dolor sint est duis sed', 0, '2018-09-24 10:52:53', '0000-00-00 00:00:00'),
(19, 3, 0, 1, 'Atque ut consequat Totam minim amet qui qui perspiciatis ab incidunt itaque in odio', 0, '2018-09-24 10:52:58', '0000-00-00 00:00:00'),
(20, 3, 0, 1, 'Esse possimus sint numquam id porro quod occaecat dolor non velit veritatis officia vel nisi', 0, '2018-09-24 10:53:06', '0000-00-00 00:00:00'),
(21, 3, 0, 10, 'Explicabo Corporis deserunt qui est quas lorem proident recusandae Fugiat repudiandae explicabo Rerum similique rerum illum est libero', 0, '2018-09-24 10:54:00', '0000-00-00 00:00:00'),
(22, 3, 0, 10, 'Explicabo Corporis deserunt qui est quas lorem proident recusandae Fugiat repudiandae explicabo Rerum similique rerum illum est libero', 0, '2018-09-24 11:04:59', '0000-00-00 00:00:00'),
(23, 3, 0, 10, 'Eius quia ut quae molestiae dolore pariatur Incididunt illum et', 0, '2018-09-24 11:05:06', '0000-00-00 00:00:00'),
(24, 3, 0, 10, 'Dolore neque possimus ut ea omnis qui est velit quia id neque sint', 0, '2018-09-24 11:05:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `quiz_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `quiz_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`quiz_id`, `course_id`, `quiz_title`) VALUES
(1, 3, 'quiz_1'),
(4, 3, 'Quiz_2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_password`, `role`, `created_at`, `update_at`, `status`) VALUES
(1, 'Clayton Keith', 'teacher@gmail.com', '123', 'teacher', '2018-09-21 16:09:45', '2018-09-24 12:49:23', 0),
(2, 'Claudia Hopkins', 'sid@gmail.com', '147', 'student', '2018-09-21 16:09:45', '2018-09-24 12:51:07', 0),
(3, 'Ella Rivas', 'vyxivudak@mailinator.com', '159', 'teacher', '2018-09-21 16:09:45', '2018-09-24 12:51:23', 0),
(4, 'Admin', 'admin@gmail.com', 'admin', 'admin', '2018-09-21 16:09:45', '2018-09-24 12:51:12', 0),
(5, 'ali', 'user@gmail.com', '123', 'student', '2018-09-21 16:09:45', '2018-09-24 12:51:30', 0),
(6, 'Nell Johnson', 'muvilan@mailinator.net', '12', 'student', '2018-09-21 16:09:45', '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`exam_id`);

--
-- Indexes for table `multiplechoices`
--
ALTER TABLE `multiplechoices`
  ADD PRIMARY KEY (`multiple_choice_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`quiz_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `exam`
--
ALTER TABLE `exam`
  MODIFY `exam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `multiplechoices`
--
ALTER TABLE `multiplechoices`
  MODIFY `multiple_choice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `quiz_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
