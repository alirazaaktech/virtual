
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Add Courses </h1>
          <p>Virual Training </p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>
      <!--    Add Page  -->
          <!-- start page -->
          <form action="<?php echo base_url()?>Course/add_course_process" method="post" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-12 ">
              <div class="tile">
                <h3 class="tile-title"> Courses </h3>
                <div class="tile-body">
                 
                    <div class="form-group row">
                      <label class="control-label col-md-3">Courses Title</label>
                      <div class="col-md-8">
                        <input class="form-control" type="text" name="course_title" placeholder="Enter Courses Title">
                      </div>
                    </div>
                </div>
                <div class="tile-footer">
                  <div class="row">
                    <div class="col-md-8 col-md-offset-3">
                     <!--  <input type="submit"  value="ADD" name="submit" class="btn btn-primary"> -->
                      <button class="btn btn-primary" type="submit"  name="submitclass">
                        <i class="fa fa-fw fa-lg fa-check-circle"></i> ADD </button>&nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="#">
                          <i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </form>
          <!-- end  page -->
      
    </main>
