<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p>A free and open source Bootstrap 4 admin template</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <div id="sampleTable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                <div class="row">
                  <div class="col-sm-12 col-md-6">
                    <div class="dataTables_length" id="sampleTable_length">
                      <label>Show 
                        <select name="sampleTable_length" aria-controls="sampleTable" class="form-control form-control-sm">
                          <option value="10">10</option>
                          <option value="25">25</option>
                          <option value="50">50</option>
                          <option value="100">100</option>
                        </select> entries
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div id="sampleTable_filter" class="dataTables_filter">
                      <label>Search:
                        <input type="search" class="form-control form-control-sm" placeholder="" aria-controls="sampleTable">
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <table class="table table-hover table-bordered dataTable no-footer" id="sampleTable" role="grid" aria-describedby="sampleTable_info">
                  
                      <thead>
                        <tr>
                        
                          <th>Name</th>
                          <th>Email</th>
                          <th>Password</th>
                          <th>Role</th>
                        </tr>
                      </thead>
                        <?php 
                           foreach ($user as $key) {
                              
                              $name=$key['user_name'];
                              $email=$key['user_email'];
                              $password=$key['user_password'];
                              $role=$key['role'];
                        ?>
                                <tr>
                                
                                 <td><?php echo $name; ?></td>
                                 <td><?php echo $email; ?></td>
                                 <td><?php echo $password; ?></td>
                                 <td><?php echo $role; ?></td>
                               </tr>
                         
                        <?php 
                            }
                        ?>
                  </table>
                  
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-5">
              <div class="dataTables_info" id="sampleTable_info" role="status" aria-live="polite">Showing 1 to 10 of 10 entries</div>
            </div>
            <div class="col-sm-12 col-md-7">
              <div class="dataTables_paginate paging_simple_numbers" id="sampleTable_paginate">
                <ul class="pagination">
                  <li class="paginate_button page-item previous disabled" id="sampleTable_previous">
                    <a href="#" aria-controls="sampleTable" data-dt-idx="0" tabindex="0" class="page-link">Previous
                    </a>
                  </li>
                  <li class="paginate_button page-item active">
                    <a href="#" aria-controls="sampleTable" data-dt-idx="1" tabindex="0" class="page-link">1
                    </a>
                  </li>
                  <li class="paginate_button page-item previous disabled" id="sampleTable_previous">
                    <a href="#" aria-controls="sampleTable" data-dt-idx="0" tabindex="0" class="page-link">Next
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
            </div>
          </div>
        </div>
      </div>

     
    </main>
