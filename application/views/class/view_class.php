  <!DOCTYPE html>

  <html>
  
 
  <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p>All Lecture</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>
      

       <div class="row">

             
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
             
                 <a href="<?php echo base_url('Teacher_class/create_class/'.$id); ?>"class="btn blue" type="button" >Add Lecture</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <a href="<?php echo base_url('Quiz/show_quiz/'.$id);?>" class="btn blue" type="button" >Quiz</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <a href="<?php echo base_url('Exam/show_exam/'.$id); ?>"" class="btn blue" type="button" >Take Exam</a>
                 <hr>
             <!--  <div id="sampleTable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="sampleTable_length"><label>Show <select name="sampleTable_length" aria-controls="sampleTable" class="form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-12 col-md-6"><div id="sampleTable_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="sampleTable"></label></div></div></div><div class="row"><div class="col-sm-12">
 -->
                <table class="table table-hover table-bordered dataTable no-footer" id="sampleTable" role="grid" aria-describedby="sampleTable_info">
              
                <thead>
                 <tr>
                  <th>Number #</th>
                    <th>Course Name </th>
                    <th>Topic Name</th>
                    <th>Detail lecture</th>
                    <th>video</th>
 
                  </tr>
                </thead>
                
                  <?php 
                       if(isset($classes)&& !empty($classes) )
                       {
                        $count = 1;
                        foreach ($classes as $key)
                        {

                          $class_id=$key['class_id'];
                          $course_title=$key['course_title'];
                          $class_topic_name=$key['class_topic_name'];

                          $class_text_document=$key['class_text_document'];
                          $video=$key['video'];
                  ?>

                              <tr>
                                <th><?php echo $count++; ?></th>
                                <td><?php echo $course_title; ?></td>
                                <td><?php echo $class_topic_name; ?></td>
                                <td><?php echo $class_text_document; ?></td>
                                <td>

                                  <video id="myVideo" width="200" height="176"  controls loop>
                                    <source src="<?php echo base_url('user_image/').$video; ?>" type="video/mp4">
                                  </video>
                                </td>
                                               
                                                  <!-- td>
                                                    <a href="<?php // echo base_url('Teacher_class/update/'.$class_id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                    <a href="<?php  //echo base_url('Teacher_class/delete/'.$class_id); ?>" class="btn blue" type="button" >Delete</a> &nbsp;
                                                </td> -->
                                             
                              </tr>
                                       
                     <?php 
                        }
                      }
                      else
                      {
                        echo "there is no Data";
                      }
                      ?>
               </table>
              
          <!--   </div></div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="sampleTable_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="sampleTable_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="sampleTable_previous"><a href="#" aria-controls="sampleTable" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="sampleTable" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="sampleTable" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="sampleTable" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="sampleTable" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item "><a href="#" aria-controls="sampleTable" data-dt-idx="5" tabindex="0" class="page-link">5</a></li><li class="paginate_button page-item "><a href="#" aria-controls="sampleTable" data-dt-idx="6" tabindex="0" class="page-link">6</a></li><li class="paginate_button page-item next" id="sampleTable_next"><a href="#" aria-controls="sampleTable" data-dt-idx="7" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>
            --> 
            </div>
          </div>
        </div>
      </div>

     
    </main>
     </html>