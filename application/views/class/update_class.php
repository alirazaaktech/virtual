
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Add Courses </h1>
          <p>Virual Training </p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>
      <!--    Add Page  -->
          <!-- start page -->
          <form action="<?php echo base_url()?>Teacher_class/update_class_process" method="post" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-12 ">
              <div class="tile">
                <h3 class="tile-title"> Courses </h3>
                <div class="tile-body">

                  <div class="form-group row">
                      <label class="control-label col-md-3">Course Name</label>
                      <div class="col-md-8 ">
                        <select name="course_id" class="form-control">
                        
                          <?php 
                              foreach ($course as $key )
                        {
                          $course_id =$key['course_id'];
                          $course_title = $key['course_title'];

                         ?>
                                    <option value="<?php echo $course_id;  ?>"><?php echo $course_title;  ?></option>
                       <?php } ?>
                      </select>
                      </div>
                    </div>
                    <?php foreach ($data as $key) {
                          $class_id =$key['class_id'];
                          $course_id = $key['course_id'];
                          $class_topic_name =$key['class_topic_name'];
                          $class_text_document = $key['class_text_document'];
                           $video =$key['video'];

                    ?>
                    <!--  <div class="form-group row">
                      <label class="control-label col-md-3">Topic Name</label>
                      <div class="col-md-8">
                        <input class="form-control" type="text"  value="<?php //echo $course_id;  ?>" readonly placeholder="Enter full name">
                      </div>
                    </div>
                  -->
                    
                     
                        <input class="form-control" type="hidden"  value="<?php echo $class_id;  ?>" readonly placeholder="Enter full name">
                      
                 

                    <div class="form-group row">
                      <label class="control-label col-md-3">Topic Name</label>
                      <div class="col-md-8">
                        <input class="form-control" type="text" name="class_topic_name" value="<?php echo $class_topic_name;  ?>" placeholder="Enter full name">
                      </div>
                    </div>

                     <div class="form-group  row">
                        <label class="control-label col-md-3">Documment</label>
                          <div class="col-md-8">
                        <textarea class="form-control rounded-0" name="class_text_document" value="<?php echo $class_text_document; ?>" id="exampleFormControlTextarea1" rows="10"><?php echo $class_text_document; ?></textarea>
                    </div>
                  </div>

                    <div class="form-group row">
                      <label class="control-label col-md-3">File Upload </label>
                      <div class="col-md-8">
                        <input class="form-control" type="file" value="<?php echo $video;  ?>" name="video">
                      </div>
                    </div>
                   <?php  } ?>
                </div>
                <div class="tile-footer">
                  <div class="row">
                    <div class="col-md-8 col-md-offset-3">
                     <!--  <input type="submit"  value="ADD" name="submit" class="btn btn-primary"> -->
                      <button class="btn btn-primary" type="submit"  name="submitclass">
                        <i class="fa fa-fw fa-lg fa-check-circle"></i> ADD </button>&nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="#">
                          <i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </form>
          <!-- end  page -->
      
    </main>
