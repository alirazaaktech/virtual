<script src="https://code.jquery.com/jquery-1.12.4.min.js">
</script>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Questions </h1>
          <p>Add Question </p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>
   
          <form action="<?php echo base_url()?>Exam/process_add" method="post" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-12 ">
              <div class="tile">
                <h3 class="tile-title"> </h3>
                <div class="tile-body">
                   <input class="form-control" type="hidden" name="exam_id" value="<?php echo $exam_id; ?>">

                  <div class="form-group row">
                      <label class="control-label col-md-2 "><h4>Course Name</h4></label>
                      <div class="col-md-10 ">
                         <?php 
                              foreach ($course as $key )
                        {
                          $course_id =$key['course_id'];                          
                          $course_title = $key['course_title'];

                         ?>

                        <td><input class="form-control" type="hidden" name="course_id" value="<?php echo $course_id; ?>">

                          <input class="form-control" type="text"  value="<?php echo $course_title;  ?>" readonly placeholder="Enter full name"></td>
                           <?php  } ?>
                      </div>
                    </div>
                     <div id="add_new_row">
                    <div class="form-group row" >
                      <label ><h4>Question</h4></label>
                      <div class="col-md-12">
                        <input class="form-control" type="text" name="question_title" required placeholder="Enter Question">
                      </div>
                    </div>

                     <div class="form-group row">  
                      <div class="col-md-4">
                        <label><h4>A</h4></label>
                        <input class="form-control" type="text" name="choice_a" required placeholder="Enter full name">
                      </div>
                      
                      <div class="col-md-4">
                         <label><h4>B</h4></label>
                        <input class="form-control" type="text" name="choice_b" required placeholder="Enter full name">
                      </div>
                      <div class="col-md-4">
                        <label><h4>C</h4></label>
                        <input class="form-control" type="text" name="choice_c" required placeholder="Enter full name">
                      </div>
                       </div>
                       <div class="form-group row"> 
                      <div class="col-md-4">
                        <label><h4>D</h4></label>
                        <input class="form-control" type="text" name="choice_d" required placeholder="Enter full name">
                      </div>
                      <div class="col-md-4">
                         <label><h4>Answer</h4></label>
                      <select class="form-control" name="answer">
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                    </select>
                      </div>
                    </div>
                  </div>
                  <!-- <div class="form-group row" >
                    <div class="col-md-2">
                    <input type="button" class="add_form btn btn-primary" value="ADD">
                  </div>
                </div> -->
                    
                      <button class="btn btn-primary" type="submit"  name="submitclass">
                        <i class="fa fa-fw fa-lg fa-check-circle"></i> ADD </button>&nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="#">
                          <i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </form>
          <!-- end  page -->
      
    </main>
<!-- 
<script type="text/javascript">
    $(document).ready(function(){

            $(document).on('click',".add_form",function(){
                var p = `
               
                   <div id="add_new_row">
                    <div class="form-group row" >
                      <label >Question</label>
                      <div class="col-md-9">
                        <input class="form-control" type="text" name="question[]" placeholder="Enter full name">
                      </div>
                      <div class="col-md-2">
                        <input type="button" class="btn btn-primary" value="remove">
                      </div>
                    </div>

                     <div class="form-group row">  
                      <div class="col-md-3">
                        <label>A</label>
                        <input class="form-control" type="text" name="choice_a[]" placeholder="Enter full name">
                      </div>
                      
                      <div class="col-md-3">
                         <label>B</label>
                        <input class="form-control" type="text" name="choice_b[]" placeholder="Enter full name">
                      </div>
                      <div class="col-md-3">
                        <label>C</label>
                        <input class="form-control" type="text" name="choice_c[]" placeholder="Enter full name">
                      </div>
                       
                      <div class="col-md-3">
                        <label>D</label>
                        <input class="form-control" type="text" name="choice_d[]" placeholder="Enter full name">
                      </div>
                    </div>
                  </div>
                `;
                $("#add_new_row").append(p);
            });

            $(document).on('click',".delete_form",function(){
                   $('#add_new_row').remove(p);
            });
        }); 
    </script> -->