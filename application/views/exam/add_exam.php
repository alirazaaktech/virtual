<script src="https://code.jquery.com/jquery-1.12.4.min.js">
</script>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Add Exam </h1>
          <p>Virual Training </p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>
   
          <form action="<?php echo base_url()?>Exam/process_add_Exam" method="post" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-12 ">
              <div class="tile">
                <h3 class="tile-title"> </h3>
                <div class="tile-body">

                   <div class="form-group row">
                      <!-- <label class="control-label col-md-2 "><h4>Course Name</h4></label> -->
                      <div class="col-md-10 "> 
                        

                        <td><input class="form-control" type="hidden" name="course_id" value="<?php echo $id; ?>">
                        </td>
                     
                      </div>
                    </div>
                    
                    <div class="form-group row" >
                      <label ><h4>Exam Title</h4></label>
                      <div class="col-md-12">
                        <input class="form-control " type="text" name="exam_title" required placeholder="Enter Exam Title">
                      </div>
                    </div>

              
                  </div>
                  <!-- <div class="form-group row" >
                    <div class="col-md-2">
                    <input type="button" class="add_form btn btn-primary" value="ADD">
                  </div>
                </div> -->
                    
                      <button class="btn btn-primary" type="submit"  name="submitclass">
                        <i class="fa fa-fw fa-lg fa-check-circle"></i> ADD </button>&nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="#">
                          <i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </form>
          <!-- end  page -->
      
    </main>
