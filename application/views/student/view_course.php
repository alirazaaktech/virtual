  <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p>All Courses</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>
      

       <div class="row">
        <div class="col-md-12">

          <div class="tile">
            <div class="tile-body">
              <br>
              
              <hr>
              <div id="sampleTable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="sampleTable_length"><label>Show <select name="sampleTable_length" aria-controls="sampleTable" class="form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-12 col-md-6"><div id="sampleTable_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="sampleTable"></label></div></div></div>
              <div class="row">
                <div class="col-sm-12">

                <table class="table table-hover table-bordered dataTable no-footer" id="sampleTable" role="grid" aria-describedby="sampleTable_info">
              
                <thead>
                 <tr>
                    <th>ID</th>
                    <th>Course Name</th>
                    
                    
                  </tr>
                </thead>
                
                       <?php 
                         if(isset($course)&& !empty($course) ){
                        $count=1;
                       foreach ($course as $key)
                       {
                  
                                $id=$key['course_id'];
                                $name=$key['course_title'];
                                
                                ?>
                                              <tr>
                                               <td><?php echo $count++; ?></td>
                                               <td> <a href="<?php echo base_url('Student/show_class/'.$id); ?>" class="btn blue" type="button" ><?php echo $name; ?></a>&nbsp;&nbsp;</td>
                                                 <td>
                                                    <a href="<?php echo base_url('Student/drop_courses/'.$id); ?>" class="btn blue" type="button" >Drop-Course</a>&nbsp;&nbsp;
                                                    <!-- <a href="<?php echo base_url('Course/delete/'.$id); ?>" class="btn blue" type="button" >Delete</a> &nbsp; -->
                                                </td>
                                               

                                              </tr>
                                       
                     <?php 
                        }
                      }
                      else
                      {
                        echo "there is no Data";
                      }
                        ?>
               </table>
              
           
            </div>
          </div>
        </div>
      </div>

     
    </main>