  <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p>Show Question</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>
      
 <form action="<?php echo base_url()?>Student/check_question_process_exam" method="post" enctype="multipart/form-data">
       <div class="row">

             
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
             
              <h2>All Question</h2>
                
               <br>
               <hr>
               
        
                                      <input type="hidden" name="course_id" value="<?php echo $course_id?>">
                                      <input type="hidden" name="exam_id" value="<?php echo $exam_id?>"> 
                <table class="table table-hover table-bordered dataTable no-footer" id="sampleTable" role="grid" aria-describedby="sampleTable_info">
              
                <thead>
                 <tr>
                   <!--  <th>Quiz ID </th> -->
                    
                   <th class="bold" width="60%"> <h4>Question Name</h4></th>
                    <th class="bold" width="40%"> <h4>Multiple Choice</h4></th>
                    
                  </tr>
                </thead>
                
                       <?php 
                         if(isset($question)&& !empty($question) ){
                         foreach ($question as $key)
                         {

                        
                          $question_id = $key['question_id'];
                          $course_id = $key['course_id'];
                          $exam_id=$key['exam_id'];
                          $exam_title=$key['exam_title'];
                          $question_title=$key['question_title'];
                          $choice_a=$key['choice_a'];
                          $choice_b=$key['choice_b'];
                          $choice_c=$key['choice_c'];
                          $choice_d=$key['choice_d'];
                          $answer=$key['answer'];

                       
                            
                                
                        ?>

                                              <tr>
            
                                              <td ><h6><?php echo $question_title; ?></h6></td>
                                              

                                                  <td>
                                                 <ul class="sub-menu">

                                                  
                                                 <input type="radio" name="role[<?php echo $question_id ; ?>]['A']" value="A"  id="role_<?php echo $question_id ; ?>"><span class="label-text"></span>&nbsp;&nbsp;&nbsp;A &nbsp;&nbsp;&nbsp; <?php echo  $choice_a; ?> &nbsp;&nbsp;<br>
                                                  
                                                  <input type="radio" name="role[<?php echo $question_id ; ?>]['B']" value="B"  id="role_<?php echo $question_id ; ?>"><span class="label-text"></span>&nbsp;&nbsp;&nbsp;B &nbsp;&nbsp;&nbsp; <?php echo  $choice_b; ?> &nbsp;&nbsp;<br>
                                              
                                                  <input type="radio" name="role[<?php echo $question_id ; ?>]['C']" value="C" id="role_<?php echo $question_id ; ?>"><span class="label-text"></span>&nbsp;&nbsp;&nbsp;C &nbsp;&nbsp;&nbsp;   <?php echo    $choice_c; ?> &nbsp;&nbsp;<br>

                                                  <input type="radio" name="role[<?php echo $question_id ; ?>]['D']" value="D" id="role_<?php echo $question_id ; ?>"><span class="label-text"></span>&nbsp;&nbsp;&nbsp;D &nbsp;&nbsp;&nbsp;   <?php echo    $choice_d; ?> &nbsp;&nbsp;
                                              <br>
                                              <br>
                                             
                                                     </ul>
                                                </td>
                                             
                                              </tr>
                                       
                     <?php 
                        }
                        ?>
                     
               </table>
                    <button class="btn btn-primary pull-right" type="submit"  name="submitclass">
                        <i class="fa fa-fw fa-lg fa-check-circle"></i> SUBMIT </button>&nbsp;&nbsp;&nbsp;
                <?php      
                    }
                    else
                    {
                        echo "there is no Data";
                    }
                ?>
            </div>
          </div>
        </div>
      </div>

    </form>

     
    </main>