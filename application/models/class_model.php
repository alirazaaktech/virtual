<?php 

include_once('abstract_model.php');
  
//protected $table_name = "";
  
class Class_model extends Abstract_model 
{

	public function __construct() 
	{
    $this->table_name = "classes";
    parent::__construct();
  }

  public function all_classes($id)
  {
    $this->db->select("*");
    $this->db->from('courses');
    $this->db->join('classes','classes.course_id = courses.course_id');
    $this->db->where('classes.course_id', $id);
    $this->db->order_by("classes.class_id","desc");
    $query = $this->db->get();
    if($query->num_rows()>0) 
      {
        return $query->result_array();
      }
      else  
      {  
          return false;  
      }  
  }

  public function show_classes()
  {
    $this->db->select("*");
    $this->db->from('courses');
     $this->db->where('status',0);
    $this->db->order_by("courses.course_id","desc");
    $query = $this->db->get();
    if($query->num_rows()>0) 
      {
        return $query->result_array();
      }
      else  
      {  
          return false;  
      }  
  }
   public function show_enroll_class()
  {
    $this->db->select("*");
    $this->db->from('courses');
     $this->db->where('status',1);
    $this->db->order_by("courses.course_id","desc");
    $query = $this->db->get();
    if($query->num_rows()>0) 
      {
        return $query->result_array();
      }
      else  
      {  
          return false;  
      }  
  }
  public function delete_class($table,$class_id)
  {
    $this->db->where('class_id', $class_id);
    $this->db->delete($table);
  }

  public function class_update($table,$class_id)
  {
    $this->db->where('class_id', $class_id);
    $query = $this->db->get($table);
    return $query->result_array(); 
  }
   

}
?>