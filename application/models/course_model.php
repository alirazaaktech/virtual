<?php 

include_once('abstract_model.php');
  
//protected $table_name = "";
  
class Course_model extends Abstract_model 
{

	public function __construct() 
	{
        $this->table_name = "";
		    parent::__construct();
    }
    
    public function delet_data($table,$user_id)
    {
        $this->db->where('user_id', $user_id);
       return $this->db->delete($table); 
    }
       public function course_show($table)
  {

    $this->db->select("*");
    $this->db->from($table);
      $query = $this->db->get();
      return $query->result_array(); 
  }
  public function enrolled_course_show($table)
  {
     $this->db->select("*");
    $this->db->from($table);
    $this->db->where('status', 1);
      $query = $this->db->get();
      return $query->result_array(); 
  }
   
    public function couser_show($table,$id)
  {
    $this->db->select("*");
    $this->db->from($table);
    $this->db->where('course_id', $id);
      $query = $this->db->get();
      return $query->result_array(); 
  }
  public function insert_course($table,$data)
  {
    $this->db->insert($table,$data);
  }
  public function delete_course($table,$course_id)
  {
    $this->db->where('course_id', $course_id);
    $this->db->delete($table);
}
public function course_update($table,$course_id)
{
    $this->db->where('course_id', $course_id);
    $query = $this->db->get($table);
      return $query->result_array(); 
}
 public function updated_course($table,$data,$course_id )
 {
    $this->db->where('course_id', $course_id );
    $this->db->update($table, $data);
 }
  


}
?>