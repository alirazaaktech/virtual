<?php

/** 
* RDA Admins Model 
*
* Model to manage Admins Table 
*
* @package 		Admin Pannel  
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://
*/

include_once('abstract_model.php');

class Login_model extends Abstract_model
{
	/**
	* @var stirng
	* @access protected
	*/
    protected $table_name = "";
	
	/** 
	*  Model constructor
	* 
	* @access public 
	*/
    public function __construct() 
	{
        $this->table_name = "users";
		parent::__construct();
    }
    public function user_login($email,$password,$role)
	{
		$this->db->select();
		$this->db->from($this->table_name);
		$this->db->where('user_email',$email);
		$this->db->where('user_password',$password);
		$this->db->where('role',$role);
		$data= $this->db->get();  
	    if($data->num_rows()>0) 
	    {
	   		return $data->result_array();
	    }
	    else
		{
			$this->session->set_flashdata('message','Invalid Email or Password ');
			redirect('Login/login');
		}
	}



     

}