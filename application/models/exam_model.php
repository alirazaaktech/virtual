<?php

/** 
* RDA Admins Model 
*
* Model to manage Admins Table 
*
* @package 		Admin Pannel  
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://
*/

include_once('abstract_model.php');

class Exam_model extends Abstract_model
{
	/**
	* @var stirng
	* @access protected
	*/
    protected $table_name = "";
	
	/** 
	*  Model constructor
	* 
	* @access public 
	*/
    public function __construct() 
	{
        $this->table_name = "questions";
		parent::__construct();
    }
   public function insert_multiple_choice($table,$data2)
   {
   		$this->db->insert($table,$data2);
   }
    public function insert_exam($table,$data)
   {
   		$this->db->insert($table,$data);
   }

   public function all_exam($table,$id)
   {
   	$this->db->where('course_id',$id);
   	$query = $this->db->get($table);
   	 return $query->result_array(); 
   }
   public function question($exam_id,$course_id)
   {
        $this->db->select();
        $this->db->from('exam');
        $this->db->join('questions','questions.exam_id=exam.exam_id');
        $this->db->join('multiplechoices','multiplechoices.question_id=questions.question_id');
        $this->db->where('questions.exam_id',$exam_id);
        $this->db->where('questions.course_id',$course_id);
        $this->db->order_by('questions.question_id','desc');
        $query = $this->db->get();
        return $query->result_array(); 
   }
   public function exam_result($user_id,$exam_id,$course_id)
  {
     $this->db->select();
     $this->db->from('results');
     $this->db->join('users',' users.user_id=results.user_id');
     $this->db->join('questions','questions.question_id=results.question_id');
     $this->db->join('exam','exam.exam_id=questions.exam_id');
     $this->db->join('multiplechoices','multiplechoices.question_id=questions.question_id');
     $this->db->where('results.user_id',$user_id);
      $this->db->where('results.exam_id',$exam_id);
      $this->db->where('results.course_id',$course_id);
        $query = $this->db->get();
        return $query->result_array(); 
  } 
   public function insert_attempt_exam($table,$attempt_data)
  {
    $this->db->insert($table,$attempt_data);
  }
   public function obtained_marks($table)
  {
     $this->db->where('status_exam',1);
       $query = $this->db->get($table);
        return $query->result_array(); 
  }
   public function attempted_exam($user_id,$exam_id)
   {
     $this->db->where('user_id',$user_id);
     $this->db->where('exam_id',$exam_id);
     $query = $this->db->get('attempt_exam');
      return $query->result_array();
        // echo $this->db->last_query(); 
        // exit;
   }


     

}