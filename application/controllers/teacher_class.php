<?php 

class Teacher_class extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('Course_model');
		$this->load->model('Class_model');
		$this->load->library('form_validation');
	}

	public function index($id)
	{
		$show_table['classes'] = $this->Class_model->all_classes($id);
		$show_table['id'] = $id;
		$this->load->view('class/view_class',$show_table);
	}

	public function show_class($id)
	{
		$show_table['classes'] = $this->Class_model->all_classes($id);
		$show_table['id'] = $id;
		$this->load->view('class/view_class',$show_table);
	}

	public function create_class($id)
	{
		$data['course'] = $this->Course_model->couser_show('courses',$id);
		$this->load->view('class/add_class',$data);
	}
	public function my_load()
	{
		$config= array();
		$config['upload_path'] = './user_image/';
		$config['allowed_types'] = 'gif|jpg|png|MP4|avi|flv|wmv|mp3';
		$this->load->library('upload',$config);
		$this->upload->do_upload('video');
		move_uploaded_file($_FILES['video']['tmp_name'],FCPATH.'user_image/'.$_FILES['video']['name']);
		$data = $this->upload->data();
		if($data) 
		{
			$video = $data['file_name']; 
			return $video;
		}
	}

	public function add_class_process()
	{
		$this->form_validation->set_rules('course_id','plz enter the Course Title','required');
		$this->form_validation->set_rules('class_topic_name','plz enter the Topic Name','required');
		$this->form_validation->set_rules('class_text_document','plz enter the Text document ','required');
		$course_id = $this->input->post('course_id');
		if ($this->form_validation->run()==false) 
		{
			redirect('Teacher_class/create_class/'.$course_id);
		}
		else
		{ 
			if ($_FILES)
			{
				$video = $this->my_load();
			}
			

			$data = array( 
			'course_id' =>$course_id,
			'class_topic_name' =>$this->input->post('class_topic_name'),
			'class_text_document' =>$this->input->post('class_text_document'),
			'video' =>$video
			);
			$this->Class_model->save($data);
			
			
			redirect('Teacher_class/show_class/'.$course_id);
		}
	}

	
	  public function delete($class_id)
    {
    	
        if (isset($class_id) && !empty($class_id)) {
            $this->Class_model->delete_class('classes',$class_id);
            $this->session->set_flashdata('success_message', 'course has been deleted successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to delete course.');
        }
       redirect('Teacher_class/show_class/');
    }

     public function update($class_id)
    {
        $data = array();

   
        if (isset($class_id) && !empty($class_id)) {
        	$data['course'] = $this->Course_model->couser_show('courses');
            $data['data'] = $this->Class_model->class_update('classes', $class_id);
            if (isset($data['data']) && !empty($data['data'])) 
            {   
                $this->load->view('class/update_class',$data);
            }
            else
            {
                $this->session->set_flashdata('error_message', 'Product not found.');
            }
        }
    }

    public function update_class_process()
    {
  //   	$this->form_validation->set_rules('course_title','plz enter the Course Title','required');
		
		// if ($this->form_validation->run()==false) 
		// {
		// 	redirect('Teacher/create_course');
		// }
		// else
		// { 
			// if ($_FILES)
			// {
			// 	$video = $this->my_load();
			// }
			// echo $class_id = $this->input->post('class_id');
			// exit; 
					// $data = array( 
					// 'course_id' =>$this->input->post('course_id'),
					// 'class_topic_name' =>$this->input->post('class_topic_name'),
					// 'class_text_document' =>$this->input->post('class_text_document'),
					// 'video' =>$video
					// );
					// $this->Course_model->updated_cousre('courses',$data,$course_id );
					// $this->success();
					// redirect('Teacher/show_course');
		//}
	}
    

	// public function success()
	// {
	// 	$this->session->set_flashdata('success', 'Successfully Record Inserted');
	// }
	
}

?>