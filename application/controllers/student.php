<?php 

class Student extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('Course_model');
		$this->load->model('Class_model');
		$this->load->model('Quiz_model');
		$this->load->model('Exam_model');

		$this->load->library('form_validation');
	}

	// public function index($id)
	// {
	// 	$show_table['classes'] = $this->Class_model->all_classes($id);
	// 	$show_table['id'] = $id;
	// 	$this->load->view('class/view_class',$show_table);
	// }

	public function show_class($course_id)
	{
		$show_table['classes'] = $this->Class_model->all_classes($course_id);
		$show_table['id'] = $course_id;
		$show_table['quiz'] = $this->Quiz_model->all_quiz('quiz',$course_id);
		$show_table['exam'] = $this->Exam_model->all_exam('exam',$course_id);

		$this->load->view('student/view_class',$show_table);
	}
	public function check_question_process()
	{	
		$quiz_id=$this->input->post('quiz_id');
		$course_id=$this->input->post('course_id');
		$users_ans=$this->input->post('role');
		 $user_id = $this->session->userdata('user_id');
		 $attempt_data = array(
		 				'quiz_id' =>$quiz_id ,
		 				'course_id' => $course_id, 
						'user_id' =>$user_id ,
		 			);
		 $this->Quiz_model->insert_attempt_quiz('attempt_quiz',$attempt_data);
		foreach ($users_ans as $question_id=>$answers) 
		{
			$std_ansr = $answers[0];
			 $isCorrect = $this->Quiz_model->check_quiz('multiplechoices',$question_id);
			$tabl_anser = $isCorrect[0]['answer'];
			$arr = array();
			$arr['user_id'] =  $user_id;
			$arr['student_answer'] = $std_ansr;
			$arr['right_answer'] = $tabl_anser;
			$arr['quiz_id'] = $quiz_id;
			$arr['course_id'] = $course_id;
			$arr['question_id']  = $question_id;
			if($std_ansr==$tabl_anser)
			{
				$arr['status_quiz'] = 1;
			}
			else
			{
				$arr['status_quiz'] = 0;
			}
			$this->Quiz_model->insert_result('results',$arr);
		}
		redirect('Student/result_show/'.$quiz_id.'/'.$course_id);
	}
	public function check_question_process_exam()
	{	
		$exam_id=$this->input->post('exam_id');
		$course_id=$this->input->post('course_id');
		$users_ans=$this->input->post('role');
		 $user_id = $this->session->userdata('user_id');
		 $attempt_data = array(
		 				'exam_id' =>$exam_id ,
		 				'course_id' => $course_id, 
						'user_id' =>$user_id ,
		 			);
		 $this->Exam_model->insert_attempt_exam('attempt_exam',$attempt_data);
		foreach ($users_ans as $question_id=>$answers) 
		{
			$std_ansr = $answers[0];
			 $isCorrect = $this->Quiz_model->check_quiz('multiplechoices',$question_id);
			$tabl_anser = $isCorrect[0]['answer'];
			$arr = array();
			$arr['user_id'] =  $user_id;
			$arr['student_answer'] = $std_ansr;
			$arr['right_answer'] = $tabl_anser;
			$arr['exam_id'] = $exam_id;
			$arr['course_id'] = $course_id;
			$arr['question_id']  = $question_id;
			if($std_ansr==$tabl_anser)
			{
				$arr['status_exam'] = 1;
			}
			else
			{
				$arr['status_exam'] = 0;
			}
			$this->Quiz_model->insert_result('results',$arr);
		}
		redirect('Student/result_show_exam/'.$exam_id.'/'.$course_id);
	}
	public function result_show($quiz_id,$course_id)
	{
		$user_id =$this->session->userdata('user_id');
		$result['quiz'] = $this->Quiz_model->specific_quiz('quiz',$quiz_id);
		$result['course'] = $this->Course_model->couser_show('courses',$course_id);
		$result['obtained_marks'] = count($this->Quiz_model->obtained_marks('results'));
		$result['total_marks'] = count($this->Quiz_model->quiz_result($user_id,$quiz_id,$course_id));
		$result['result'] = $this->Quiz_model->quiz_result($user_id,$quiz_id,$course_id);

		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('student/view_quiz_result',$result);
		// exit;
	}
	public function result_show_exam($exam_id,$course_id)
	{
		$user_id =$this->session->userdata('user_id');
		$result['course'] = $this->Course_model->couser_show('courses',$course_id);
		$result['obtained_marks'] = count($this->Exam_model->obtained_marks('results'));
		$result['total_marks'] = count($this->Exam_model->exam_result($user_id,$exam_id,$course_id));
		$result['result'] = $this->Exam_model->exam_result($user_id,$exam_id,$course_id);

		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('student/view_exam_result',$result);
		// exit;
	}

	public function show_question_quiz($quiz_id,$course_id)
	{	
		$data['quiz_id']= $quiz_id;
		$data['course_id']=$course_id;
		 $data['question'] = $this->Quiz_model->question($quiz_id,$course_id);

		$this->load->view('student/veiw_question',$data);
	}
	public function show_question_exam($exam_id,$course_id)
	{	
		$data['exam_id']= $exam_id;
		$data['course_id']=$course_id;
		$data['question'] = $this->Exam_model->question($exam_id,$course_id);

		$this->load->view('student/view_question_exam',$data);
	}

	public function show_course()
	{
		$show_table['classes'] = $this->Class_model->show_classes();
		$this->load->view('student/viewcourse',$show_table);
	}
	public function enrolled_course()
	{
		$show_table['course'] = $this->Course_model->enrolled_course_show('courses');
		$this->load->view('student/view_course',$show_table);
	}
	public function enroll_courses($id)
	{
	     $data= array('status' => 1 );
<<<<<<< HEAD

=======
>>>>>>> 1d37533df4ce471776d673b85ec84c7218101bb9
	     $this->Course_model->updated_course('courses',$data,$id);
		 $show_table['enrollcourses'] = $this->Class_model->show_enroll_class();
		 $this->load->view('student/enrollcourses',$show_table);
	}
    public function drop_courses($id)
	{
	     $data= array('status' => 0 );
	     $this->Course_model->updated_course('courses',$data,$id);
		 $show_table['enrollcourses'] = $this->Class_model->show_enroll_class();
		 $this->load->view('student/enrollcourses',$show_table);
	}
	
	public function my_load()
	{
		$config= array();
		$config['upload_path'] = './user_image/';
		$config['allowed_types'] = 'gif|jpg|png|MP4|avi|flv|wmv|mp3';
		$this->load->library('upload',$config);
		$this->upload->do_upload('video');
		move_uploaded_file($_FILES['video']['tmp_name'],FCPATH.'user_image/'.$_FILES['video']['name']);
		$data = $this->upload->data();
		if($data) 
		{
			$video = $data['file_name']; 
			return $video;
		}
	}

	public function add_class_process()
	{
		$this->form_validation->set_rules('course_id','plz enter the Course Title','required');
		$this->form_validation->set_rules('class_topic_name','plz enter the Topic Name','required');
		$this->form_validation->set_rules('class_text_document','plz enter the Text document ','required');
		$course_id = $this->input->post('course_id');
		if ($this->form_validation->run()==false) 
		{
			redirect('Teacher_class/create_class/'.$course_id);
		}
		else
		{ 
			if ($_FILES)
			{
				$video = $this->my_load();
			}
			

			$data = array( 
			'course_id' =>$course_id,
			'class_topic_name' =>$this->input->post('class_topic_name'),
			'class_text_document' =>$this->input->post('class_text_document'),
			'video' =>$video
			);
			$this->Class_model->save($data);
			
			
			redirect('Teacher_class/show_class/'.$course_id);
		}
	}

	
	  public function delete($class_id)
    {
    	
        if (isset($class_id) && !empty($class_id)) {
            $this->Class_model->delete_class('classes',$class_id);
            $this->session->set_flashdata('success_message', 'course has been deleted successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to delete course.');
        }
       redirect('Teacher_class/show_class/');
    }

     public function update($class_id)
    {
        $data = array();

   
        if (isset($class_id) && !empty($class_id)) {
        	$data['course'] = $this->Course_model->couser_show('courses');
            $data['data'] = $this->Class_model->class_update('classes', $class_id);
            if (isset($data['data']) && !empty($data['data'])) 
            {   
                $this->load->view('class/update_class',$data);
            }
            else
            {
                $this->session->set_flashdata('error_message', 'Product not found.');
            }
        }
    }

    public function update_class_process()
    {
  //   	$this->form_validation->set_rules('course_title','plz enter the Course Title','required');
		
		// if ($this->form_validation->run()==false) 
		// {
		// 	redirect('Teacher/create_course');
		// }
		// else
		// { 
			// if ($_FILES)
			// {
			// 	$video = $this->my_load();
			// }
			// echo $class_id = $this->input->post('class_id');
			// exit; 
					// $data = array( 
					// 'course_id' =>$this->input->post('course_id'),
					// 'class_topic_name' =>$this->input->post('class_topic_name'),
					// 'class_text_document' =>$this->input->post('class_text_document'),
					// 'video' =>$video
					// );
					// $this->Course_model->updated_cousre('courses',$data,$course_id );
					// $this->success();
					// redirect('Teacher/show_course');
		//}
	}
    

	// public function success()
	// {
	// 	$this->session->set_flashdata('success', 'Successfully Record Inserted');
	// }
	
}

?>