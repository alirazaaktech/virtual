<?php 

class Quiz extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('Course_model');
		$this->load->model('Class_model');
		$this->load->model('Quiz_model');
		$this->load->library('form_validation');
	}
	public function show_quiz($id)
	{
		$data['quiz'] = $this->Quiz_model->all_quiz('quiz',$id);
		$data['id'] = $id;
		$this->load->view('quiz/view_quiz',$data);
	}

	public function add_quiz($id)
	{
		$data['id'] = $id;
		$data['course'] = $this->Course_model->couser_show('courses',$id);
		$this->load->view('quiz/add_quiz',$data);
	}
	
	public function process_add_quiz()
	{
		$this->form_validation->set_rules('quiz_title','plz enter the Quiz Title ','required');
		$course_id =  $this->input->post('course_id');

		if ($this->form_validation->run()==false) 
		{
			redirect('Quiz/add_quiz/'.$course_id);
		}
		else
		{
			$data = array(
			'course_id' => $course_id,
			'quiz_title' => $this->input->post('quiz_title')
		);

		$this->Quiz_model->insert_quiz('quiz',$data);
		redirect('Quiz/show_quiz/'.$course_id);
		}
	}
	public function show_question($quiz_id,$course_id)
	{	
		$data['quiz_id']= $quiz_id;
		$data['course_id']=$course_id;
		 $data['question'] = $this->Quiz_model->question($quiz_id,$course_id);

		$this->load->view('quiz/view_question',$data);
	}


	public function add_question($quiz_id,$course_id)
	{
		$data['course'] = $this->Course_model->couser_show('courses',$course_id);
		$data['quiz_id'] = $quiz_id;
		$this->load->view('quiz/add_question',$data);
	}

	public function process_add()
	{
		$this->form_validation->set_rules('question_title','plz enter the Question Title','required');
		$this->form_validation->set_rules('choice_a','plz enter the Choice A ','required');
		$this->form_validation->set_rules('choice_b','plz enter the Choice B','required');
		$this->form_validation->set_rules('choice_c','plz enter the Choice C','required');
		$this->form_validation->set_rules('choice_d','plz enter the Choice D ','required');
		$course_id =  $this->input->post('course_id');
		$quiz_id =  $this->input->post('quiz_id');

		if ($this->form_validation->run()==false) 
		{
			redirect('Quiz/add_question/'.$quiz_id.'/'.$course_id);
		}
		else
		{
			$this->db->trans_start();
		$data = array(
			'course_id' => $course_id,
			'quiz_id' => $quiz_id,
			'question_title' => $this->input->post('question_title'),
		);

		$this->Quiz_model->save($data);
		$question_id = $this->db->insert_id();

		$data2=array(
			'question_id' => $question_id,
			'choice_a' => $this->input->post('choice_a'),
			'choice_b'=>$this->input->post('choice_b'),
			'choice_c'=>$this->input->post('choice_c'),
			'choice_d'=>$this->input->post('choice_d'),
			'answer'=>$this->input->post('answer')
		);
		$this->Quiz_model->insert_multiple_choice('multiplechoices',$data2);
		$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
				
			}
			redirect('Quiz/show_question/'.$quiz_id.'/'.$course_id);

		}

		

	}
	     
	  
	  


}

?>