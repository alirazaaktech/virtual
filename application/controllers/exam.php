<?php 

class Exam extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('Course_model');
		$this->load->model('Class_model');
		$this->load->model('Exam_model');
		$this->load->library('form_validation');
	}
	public function show_exam($id)
	{
		$data['exam'] = $this->Exam_model->all_exam('exam',$id);
		$data['id'] = $id;
		$this->load->view('exam/view_exam',$data);
	}

	public function add_exam($id)
	{
		$data['id'] = $id;
		$data['course'] = $this->Course_model->couser_show('courses',$id);
		$this->load->view('exam/add_exam',$data);
	}
	
	public function process_add_Exam()
	{
		$this->form_validation->set_rules('exam_title','plz enter the Exam Title ','required');
		$course_id =  $this->input->post('course_id');

		if ($this->form_validation->run()==false) 
		{
			redirect('Exam/add_exam/'.$course_id);
		}
		else
		{
			$data = array(
			'course_id' => $course_id,
			'exam_title' => $this->input->post('exam_title')
		);

		$this->Exam_model->insert_exam('exam',$data);
		redirect('Exam/show_exam/'.$course_id);
		}
	}
	public function show_question($exam_id,$course_id)
	{	
		$data['exam_id']= $exam_id;
		$data['course_id']=$course_id;
		$data['question'] = $this->Exam_model->question($exam_id,$course_id);

		$this->load->view('exam/view_question',$data);
	}


	public function add_question($exam_id,$course_id)
	{
		$data['course'] = $this->Course_model->couser_show('courses',$course_id);
		$data['exam_id'] = $exam_id;
		$this->load->view('exam/add_question',$data);
	}

	public function process_add()
	{
		$this->form_validation->set_rules('question_title','plz enter the Question Title','required');
		$this->form_validation->set_rules('choice_a','plz enter the Choice A ','required');
		$this->form_validation->set_rules('choice_b','plz enter the Choice B','required');
		$this->form_validation->set_rules('choice_c','plz enter the Choice C','required');
		$this->form_validation->set_rules('choice_d','plz enter the Choice D ','required');
		$course_id =  $this->input->post('course_id');
		$exam_id =  $this->input->post('exam_id');

		if ($this->form_validation->run()==false) 
		{
			redirect('Exam/add_question/'.$exam_id.'/'.$course_id);
		}
		else
		{
			$this->db->trans_start();
		$data = array(
			'course_id' => $course_id,
			'exam_id' => $exam_id,
			'question_title' => $this->input->post('question_title'),
		);

		$this->Exam_model->save($data);
		$question_id = $this->db->insert_id();

		$data2=array(
			'question_id' => $question_id,
			'choice_a' => $this->input->post('choice_a'),
			'choice_b'=>$this->input->post('choice_b'),
			'choice_c'=>$this->input->post('choice_c'),
			'choice_d'=>$this->input->post('choice_d'),
			'answer'=>$this->input->post('answer')
		);
		$this->Exam_model->insert_multiple_choice('multiplechoices',$data2);
		$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
				
			}
			redirect('exam/show_question/'.$exam_id.'/'.$course_id);

		}

		

	}
	     
	  
	  


}

?>