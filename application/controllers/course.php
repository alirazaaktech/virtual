<?php 

class Course extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('Course_model');
		$this->load->library('form_validation');
	}
	public function index()
	{
		$show_table['course'] = $this->Course_model->course_show('courses');
		$this->load->view('course/view_course',$show_table);
	}

	public function show_course()
	{
		$show_table['course'] = $this->Course_model->course_show('courses');

		$this->load->view('course/view_course',$show_table);
	}

	public function create_course()
	{
		$this->load->view('course/add_course');
	}
	public function add_course_process()
	{
		$this->form_validation->set_rules('course_title','plz enter the Course Title','required');
		
		if ($this->form_validation->run()==false) 
		{
			redirect('Course/create_course');
		}
		else
		{ 
	
			$data = array( 
			'course_title' =>$this->input->post('course_title')
			);
			$this->Course_model->insert_course('courses',$data);
			$this->success();
			redirect('Course/show_course');
		}
	}

	
	  public function delete($course_id)
    {
        if (isset($course_id) && !empty($course_id)) {
            $this->Course_model->delete_course('courses',$course_id);
            $this->session->set_flashdata('success_message', 'course has been deleted successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to delete course.');
        }
        redirect('Course/show_course');
    }

     public function update($course_id)
    {
        $data = array();

   
        if (isset($course_id) && !empty($course_id)) {
            $data['data'] = $this->Course_model->course_update('courses', $course_id);
            if (isset($data['data']) && !empty($data['data'])) 
            {   
                $this->load->view('course/update_course',$data);
            }
            else
            {
                $this->session->set_flashdata('error_message', 'Product not found.');
            }
        }
     
    }

    public function update_course_process()
    {
    	$this->form_validation->set_rules('course_title','plz enter the Course Title','required');
		
		if ($this->form_validation->run()==false) 
		{
			redirect('Course/create_course');
		}
		else
		{ 
			$course_id = $this->input->post('course_id');
					$data = array( 
					'course_title' =>$this->input->post('course_title')
					);
					$this->Course_model->updated_course('courses',$data,$course_id );
					$this->success();
					redirect('Course/show_course');
		}
	}
    

	public function success()
	{
		$this->session->set_flashdata('success', 'Successfully Record Inserted');
	}
	
}

?>