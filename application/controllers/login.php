<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('Login_model');
		$this->layout = 'admin/dashboard';
	}

	public function index()
	{
		$this->layout='';
		$this->load->view('auth/login');
	}

	public function login()
	{
		$this->layout='';
		$this->load->view('auth/login');
	}

	public function login_process()
	{  
        $this->layout = '';
		$email=$this->input->post('email');
		$password=$this->input->post('password');
		$role = $this->input->post('role');

		$data=$this->Login_model->user_login($email,$password,$role);
		
		if($data)
		{
      		$sessiondata =  array
           		(
					'user_email' =>$data[0]['user_email'],
					'role' =>$data[0]['role'],
					'user_id' =>$data[0]['user_id'],
					'user_name' =>$data[0]['user_name'],
					'image' =>$data[0]['image']
            	);
            
		        $this->session->set_userdata($sessiondata);
		        $this->load->view('layouts/admin/dashboard',$sessiondata);
                redirect('Dashboard/Dashboard');
     	}
     	else
     	{
     		$this->session->set_flashdata('message','Invalid email');
     	}
	}

	public function unset_session()
	{
         $this->session->unset_userdata('user_email');
         $this->session->unset_userdata('user_id');
         $this->session->unset_userdata('role');
         
        redirect('Login/login');
	}
  

  	
}
