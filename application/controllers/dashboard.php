<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{

	public function __construct()
	{
	    parent::__construct();
		$this->layout = 'admin/dashboard';
	}


	public function index()
	{
		$this->load->view('dashboard/dashboard');
	}
	public function dashboard()
	{
		$this->load->view('dashboard/dashboard');
	}
}
