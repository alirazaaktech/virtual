<?php 

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('user_model');
		$this->load->library('form_validation');
	}
	public function index()
	{
		$show_table['user'] = $this->user_model->user_show('users');
		$this->load->view('user/view',$show_table);
	}

	public function add_process()
	{
		$this->form_validation->set_rules('name',' Name','required');
		$this->form_validation->set_rules('email',' Email','required|valid_email|is_unique[users.user_email]');
		$this->form_validation->set_rules('password',' Password','required');
		$this->form_validation->set_rules('confirm_password',' Password','required');
		$this->form_validation->set_rules('role','Role','required');
		if ($this->form_validation->run()==false) 
		{
			redirect('Login/login');
		}
		else
		{ 
			$Password =$this->input->post('password');
			$confirm_password =$this->input->post('confirm_password');
				if($Password == $confirm_password)
				{
					$data = array( 
					'user_name' =>$this->input->post('name'),
					'user_email' =>$this->input->post('email'),
					'user_password' =>$Password,
					'role' => $this->input->post('role'),
					);
					$this->user_model->insert_user($data);
					$this->success();
					redirect('Login/login');
				}
				else
				{
					$this->session->set_flashdata('error_message', 'Confirm Password does Not Match');
					redirect('Login/login');

				}
		}	
	}

	
	public function success()
	{
		$this->session->set_flashdata('success', 'Successfully Record Inserted');
	}
	
}

?>